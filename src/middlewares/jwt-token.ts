import jwt from "jsonwebtoken";

export class JwtToken {
	public static checkAccessToken(req, res, next) {
		const authHeader = req.header("authorization");
        if (!!authHeader) {
            const bearerToken = authHeader.split(" ")[1];
            if (!bearerToken) {
                return res.status(401).send({error: "Unauthorized!", status: res.statusCode})
            }

            try {
                const decoded = jwt.verify(bearerToken, process.env.JWT_KEY);
                req.user = decoded;
                next();
            } catch (error) {
                res.status(401).send({error: "Invalid token", status: res.statusCode});
            }
        } else {
            return res.status(400).send({error: "Token is missing", status: res.statusCode});
        }
	}
}