import { Request, Response } from "express";

// models
import User from "../models/user";
import Tweet from "../models/tweet";

export class TweetService {
	constructor(private tweetModel) {}

	public async listAll() {
		try {
			let tweets = await this.tweetModel.find();
			if (tweets.length > 0) {
				return { tweets, status: 200 };
			}
			return { error: "No tweets found!", status: 404 };
		} catch(err) {
			return { error: "Something happened!", status: 500 };
		}	
	}

	public async myList(username: string) {
		try {
			let myTweets = await this.tweetModel.find({ username });
			if (myTweets.length > 0) {
				return { tweets: myTweets, status: 200 };
			}
			return { error: "No tweets found!", status: 404 };
		} catch(err) {
			return { error: "Something happened!", status: 500 };
		}
	}

	public static async listUserTweets(req, res: Response, next) {
		try {
			let userTweets = await Tweet.find({ username: req.params.username });
			return res.status(200).send({ tweets: userTweets, status: res.statusCode });
		} catch(err) {
			return res.status(500)
				.send({ error: "Something happened!", status: res.statusCode });
		}
	}

	public static async publishTweet(req, res: Response, next) {
		try {
			const user = await User.findOne({ username: req.user.username });
			let { username, fullName, email, photo } = user;
			let data = {
				...req.body, 
				...{ username, fullName, email, photo }
			};
			const tweet = new Tweet(data);
			await tweet.save();
			user.tweets.push(tweet);
			await user.save();
			return res.status(201)
				.send({ tweet, status: res.statusCode });
		} catch(err) {
			console.error(err);
			return res.status(500)
				.send({ error: "Something happened!", status: res.statusCode });
		}
	}
}