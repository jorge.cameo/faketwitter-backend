import { Request, Response } from "express";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
// models
import User from "../models/user";

export class AuthService {
  constructor (private userModel) {}

  public async signin(body: any) {
    try {
      let { username, password } = body;
      const JWT_KEY = process.env.JWT_KEY || "securekey";
      if (!username || !password) {
        return { error: "Missing credentials", status: 400 };
      } else {
        let user = await this.userModel.findOne({username: username});
        if (!user) {
          return {error: "User doesn't exist!", status: 401 };
        } else if (user && await bcrypt.compare(password, user.password)) {
          const token = jwt.sign(
            { user_id: user.id, username },
            JWT_KEY, { expiresIn: "2h" }
          );
          return { accessToken: token, user, status: 201 };
        } else {
          return { error: "Wrong credentials!", status: 401 };
        }
      }
    } catch (err) {
      console.error(err);
      return { error: "Something happened!", status: 500 };
    }
  }

	public async signup(body: any) {
    try {
      let { username, fullName, photo, email, password } = body;
      let userExists = await this.userModel.findOne({ $or: [{ username: username}, {email: email}] });
      if (userExists) {
        return {
          error: "User already exists with same username or email",
          status: 409
        };
      };
      let encryptedPassword = await bcrypt.hash(password, 10);
      const user = await this.userModel.create({ username, fullName, photo, email, password: encryptedPassword });
      return { user, status: 201 };
    } catch(err) {
      console.error("[api/auth]", err);
      return { error: "Something happened!", status: 500 };
    }
  }
}