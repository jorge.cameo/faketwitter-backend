import { AuthService } from "../auth";

import { UserMock } from "../../mocks/user";

jest.setTimeout(60000);

/*beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});*/

describe("auth", () => {
	// login
	it("signin method - Signin a registered user : Should return 201", async () => {
		let response = new AuthService(UserMock);
		let credentials = {username: "jorgeluis", password: "31mayo1991"};
		let method = await response.signin(credentials);
		expect(method).toEqual(
			{ status: 201, accessToken: expect.any(String), user: expect.any(Object) }
		);
	});
	it("signin method - Signin with non registered user : Should return 401", async () => {
		let response = new AuthService(UserMock);
		let credentials = {username: "jorgeluiss", password: "31mayo1991"};
		let method = await response.signin(credentials);
		expect(method).toEqual({ status: 401, error: expect.any(String) });
	});
	it("signin method - Signin with missing credentials : Should return 400", async () => {
		let response = new AuthService(UserMock);
		let method = await response.signin({});
		expect(method).toEqual({ status: 400, error: expect.any(String), });
	});
	it("signin method - Signin a registered user with wrong credentials : Should return 401", async () => {
		let response = new AuthService(UserMock);
		let credentials = {username: "jorgeluis", password: "31mayo1991."};
		let method = await response.signin(credentials);
		expect(method).toEqual({ status: 401, error: expect.any(String), });
	});

	// signup
	it("signup method - Signup a new user : Should return 201", async () => {
		let response = new AuthService(UserMock);
		let payload = {
			username: "jorge", 
			password: "pass123", 
			email: "jorge@mail.com",
			fullName: "Jorge Cameo",
			photo: "pic-2.png"
		};
		let method = await response.signup(payload);
		expect(method).toEqual({ status: 201, user: expect.any(Object) });
	});
	it("signup method - Signup a user with exsistent username/email : Should return 409", async () => {
		let response = new AuthService(UserMock);
		let payload = {
			username: "jorgeluis", 
			fullName: "Jorge Luis",
			photo: "pic.png",
			email: "jorgel6m02@gmail.com",
			password: "31mayo1991", 
		};
		let method = await response.signup(payload);
		expect(method).toEqual({ status: 409, error: expect.any(String), });
	});
});