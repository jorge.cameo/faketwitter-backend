import { TweetService } from "../tweet";

import { TweetMock } from "../../mocks/tweet";

jest.setTimeout(60000);

/*beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});*/

describe("tweet", () => {
	// list
	it("listAll method - List all tweets : Should return 200", async () => {
		let response = new TweetService(TweetMock);
		let method = await response.listAll();
		expect(method).toEqual(
			{ status: 200, tweets: expect.any(Array) }
		);
	});
	it("myList method - List all my tweets : Should return 200", async () => {
		let response = new TweetService(TweetMock);
		let method = await response.myList("jorgeluis");
		expect(method).toEqual(
			{ status: 200, tweets: expect.any(Array) }
		);
	});
	it("myList method - No tweets found : Should return 404", async () => {
		let response = new TweetService(TweetMock);
		let method = await response.myList("jorgeluisss");
		expect(method).toEqual(
			{ status: 404, error: expect.any(String) }
		);
	});
});