import { UserService } from "../user";

import { UserMock } from "../../mocks/user";

jest.setTimeout(60000);

/*beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});*/

describe("user", () => {
	// retrieves
	it("retrieve method - Retrieve a user : Should return 200", async () => {
		let response = new UserService(UserMock);
		let retrieveUserMethod = await response.retrieve("jorgeluis");
		expect(retrieveUserMethod).toEqual({status: 200, user: expect.any(Object)});
	});
	it("retrieve method - Retrieve a user that doesn't exist : Should return 404", async () => {
		let response = new UserService(UserMock);
		let retrieveUserMethod = await response.retrieve("asd");
		expect(retrieveUserMethod).toEqual({status: 404, error: expect.any(String)});
	});

	// lists
	it("listAll method - List all users : Should return 200", async () => {
		let response = new UserService(UserMock);
		let retrieveUserMethod = await response.listAll();
		expect(retrieveUserMethod).toEqual({status: 200, users: expect.any(Array)});
	});
	it("list method - List users except selected : Should return 200", async () => {
		let response = new UserService(UserMock);
		let retrieveUserMethod = await response.list("jorgeluis");
		expect(retrieveUserMethod).toEqual({status: 200, users: expect.any(Array)});
	});
	it("list method - List users except unexistent user : Should return 404", async () => {
		let response = new UserService(UserMock);
		let retrieveUserMethod = await response.list("jorgeluiss");
		expect(retrieveUserMethod).toEqual({status: 404, error: expect.any(String)});
	});

});