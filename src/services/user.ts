import { Request, Response } from "express";
// models
import User from "../models/user";
import Following from "../models/following";

export class UserService {

	constructor(private userModel) { }

	public async retrieve(username: string) {
		try {
			let user = await this.userModel.findOne({ username: username });
			if (user) {
				return { user, status: 200 };
			}
			return { error: "Not user found!", status: 404 };
		} catch(err) {
			console.error("[api/user]", err);
			return { error: "Something happened!", status: 500 };
		}
	}

	public async listAll() {
		try {
			let users = await this.userModel.find();
			return { users , status: 200 };
		} catch(err) {
			console.error("[api/user]", err);
			return { error: "Something just happend!", status: 500};
		}
	}

	public async list(username: string) {
		try {
			let user = await this.userModel.findOne({username});
			if (user) {
				let users = await this.userModel.find({ username: { $ne: username }});
				return { users , status: 200 };
			}
			return { error: "Selected user doesn't exist!", status: 404 };
		} catch(err) {
			console.error("[api/user]", err);
			return { error: "Something just happend!", status: 500};
		}
	}

	public static async follow(currentUsername: string, body: any) {
		try {
			let user = await User.findOne({ username: currentUsername })
			let { username, fullName, photo, email } = body;
			let followedUser = new Following({ username, fullName, photo, email });
			await followedUser.save();
			await user.followings.push(followedUser);
			return { user: followedUser, status: 201 };
		} catch(err) {
			return { error: "Something happend!", status: 500 };
		}
	}

	public static async followingToList(username: string) {
		try {
			let followingToUsers = await Following.find({username: username})
			return {users: followingToUsers, status: 200};
		} catch(err) {
			console.error(err);
			return { error: "Something happened!", status: 500 };
		}
	}

	public static async followingList(username: string) {
		try {
			let followingUsers = await Following.find({ "username": { $ne: username }});
			return { users: followingUsers, status: 200 };
		} catch(err) {
			console.error(err);
			return { error: "Something happened!", status: 500 };
		}
	}
}