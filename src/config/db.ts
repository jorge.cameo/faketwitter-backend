import { Mongoose } from "mongoose";

export class DbConnection {
	DB_USER: string = process.env.DB_USER;
	DB_PASS: string = process.env.DB_PASS;

	public async connect() {
		let connString = `mongodb+srv://${this.DB_USER}:${this.DB_PASS}@cluster0.natcw1k.mongodb.net/?retryWrites=true&w=majority`;
		return this.mongoose.connect(connString);
	}

	constructor(private mongoose: Mongoose) {}
}