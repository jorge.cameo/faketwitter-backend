import http from "http";
import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config();
//
import app from "./app";

const server = http.createServer(app);

const PORT = 3000;

server.listen(PORT, () => {
	console.log(`Listening at http://localhost:${PORT}`)
});