import { model, Schema } from "mongoose";

import IUser from "./iuser";

const userSchema = new Schema<IUser>({
	fullName: { type: String, required: true },
	username: { type:String, required: true, unique: true },
	email: { type: String, required: true, unique: true },
	photo: { type: String, required: true },
	password: { type: String, required: true },
	tweets: [{ type: Schema.Types.ObjectId, ref: "Tweet" }],
	followings: [{ type: Schema.Types.ObjectId, ref: "Following" }]
});

const User = model<IUser>("User", userSchema);

export default User;