import { model, Schema } from "mongoose";

interface IFollowing {
	user: any;
	username?: string;
	fullName?: string;
	photo?: string;
	isFollowing?: boolean;
}

const followingSchema = new Schema<IFollowing>({
	user: { type: Schema.Types.ObjectId, ref: "User" },
	username: { type: String, required: true },
	fullName: { type: String, required: true },
	photo: { type: String, required: true },
	isFollowing: { type: Boolean, default: false }
});

const Following = model<IFollowing>("Following", followingSchema);

export default Following;