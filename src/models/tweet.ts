import { model, Schema } from "mongoose";

interface ITweet {
	user: any;
	message: string;
	image?: string;
	username?: string;
	fullName?: string;
	photo?: string;
}

const tweetSchema = new Schema<ITweet>({
	user: { type: Schema.Types.ObjectId, ref: "User" },
	message: { type: String, required: true },
	image: { type: String },
	username: { type: String, required: true },
	fullName: { type: String, required: true },
	photo: { type: String, required: true },
});

const Tweet = model<ITweet>("Tweet", tweetSchema);

export default Tweet;