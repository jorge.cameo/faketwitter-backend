interface IUser {
	fullName: string;
	username: string;
	email: string;
	photo: string;
	password: string;
	tweets?: any[];
	followings?: any[];
}

export default IUser;