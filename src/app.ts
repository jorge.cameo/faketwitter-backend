import express from "express";
import cors from "cors";
import Mongoose from "mongoose";
// api
import authRoutes from "./api/auth";
import userRoutes from "./api/user";
import tweetRoutes from "./api/tweet";
// db
import { DbConnection } from "./config/db";

const app = express();

let dbConnect = new DbConnection(Mongoose);
dbConnect.connect()
	.then(
		res => {
			console.log("[db] DB Connected!");
		},
		err => {
			console.error("[db]", err);
		}
	)

app.use(cors({origin: "*"}));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/api/v1/auth", authRoutes);
app.use("/api/v1/user", userRoutes);
app.use("/api/v1/tweet", tweetRoutes);

export default app;