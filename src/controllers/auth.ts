import { Request, Response } from "express";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
// models
import User from "../models/user";
// services
import { AuthService } from "../services/auth";

export class AuthController {

  public static login(req: Request, res: Response, next): void {
    let service = new AuthService(User);
    service.signin(req.body)
      .then(
        result => {
          res.status(result.status).send(result);
        },
        err => {
          res.status(err.status).send(err);
        }
      )
  }

	public static signup(req: Request, res: Response, next): void {
    let service = new AuthService(User);
    service.signup(req.body)
      .then(
        result => {
          res.status(result.status).send(result);
        },
        err => {
          res.status(err.status).send(err);
        }
      )
	}
}