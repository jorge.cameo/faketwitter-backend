import { Request, Response } from "express";
// services
import { TweetService } from "../services/tweet";
// models
import Tweet from "../models/tweet";

export class TweetController {
	public static listAll(req: Request, res: Response, next): void {
		let service = new TweetService(Tweet);
		service.listAll()
		.then(
			result => {
				res.status(result.status).send(result);
			},
			err => {
				res.status(err.status).send(err);
			}
		);
	}

	public static myList(req, res: Response, next): void {
		let service = new TweetService(Tweet);
		service.myList(req.user.username)
			.then(
				result => {
					res.status(result.status).send(result);
				},
				err => {
					res.status(err.status).send(err);
				}
			);	
	}

	public static userList(req, res: Response, next): void {
		let service = new TweetService(Tweet);
		service.myList(req.params.username)
			.then(
				result => {
					res.status(result.status).send(result);
				},
				err => {
					res.status(err.status).send(err);
				}
			);	
	}
}