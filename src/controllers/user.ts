import { Request, Response } from "express";
// services
import { UserService } from "../services/user";
// models
import User from "../models/user";

export class UserController {

	public static retrieveUser (req, res: Response, next): void {
		let service = new UserService(User);
		service.retrieve(req.params.username)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static getAllList(req, res: Response, next): void {
		let userService = new UserService(User);
		userService.listAll()
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static getList(req, res: Response, next): void {
		let userService = new UserService(User);
		userService.list(req.user.username)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static getMe(req, res: Response, next): void {
		let userService = new UserService(User);
		userService.retrieve(req.user.username)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static getFollowingToList(req, res: Response, next): void {
		UserService.followingToList(req.user.username)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static getFollowingList(req, res: Response, next){
		UserService.followingList(req.user.username)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}

	public static followUser(req, res: Response, next) {
		UserService.follow(req.user.username, req.body)
			.then(
				result => {
					res.status(200).send(result);
				},
				err => {
					res.status(500).send(err);
				}
			);
	}
}