interface ITweetMock {
	id?: number;
	message?: string;
	image?: string;
	username?: string;
	fullName?: string;
	photo?:string;
}

export class TweetMock {
	static tweets?: ITweetMock[] = [
		{
			id: 1,
			message: "Tweet#1",
			image: "image-1",
			username: "jorgeluis",
			fullName: "Jorge Luis",
			photo:"photo-1.png",
		},
		{
			id: 2,
			message: "Tweet#2",
			image: "image-2",
			username: "kokimaniac",
			fullName: "Koki Maniac",
			photo:"photo-2.png",
		},
	];

	public static async find(payload = null) {
		if (payload) {
			return await this.tweets.filter(tweet => tweet.username === payload.username);
		}
		return await this.tweets;
	}
}