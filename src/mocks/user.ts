interface IUserMock {
	id?: number,
	username?: string;
	fullName?: string;
	password?: string;
	email?: string;
	photo?: string;
}

export class UserMock {
	static users: IUserMock[] = [
		{
			id: 1, 
			username: "jorgeluis", 
			fullName: "Jorge Luis", 
			password: "$2a$10$5atJiJEYgvs4pbbDgLNWF.ourug8AxlWruvgLIpvZcGpIj2xIjDQe",
			email: "jorgel6m02@gmail.com",
		}, 
		{
			id: 2, 
			username: "kokimaniac", 
			fullName: "Koki Maniac", 
			password: "$2a$10$VJlwreOLabYTOk2idsYzp.SslUn22Cbd8sGVbEEuKGxpd6YcCfdRS",
			email: "cokimaniac@gmail.com"
		},
	];
		
	public static async findOne(inputUser) {
		if (inputUser.$or) {
			return await this.users
				.find(user => user.username === inputUser.$or[0].username || user.email === inputUser.$or[1].email)
		}
		let user = await this.users.find(user => user.username === inputUser.username);
		return user;
	}

	public static async find() {
		return await this.users;		
	}

	public static async create(inputUser) {
		let id = this.users.length+1;
		let user = {...inputUser, ...{ id }};
		await this.users.push(user);
		return this.users[this.users.length-1];
	}
}