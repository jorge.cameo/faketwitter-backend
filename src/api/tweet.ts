import express from "express";
// service
import { TweetService } from "../services/tweet";
import { TweetController } from "../controllers/tweet";

import { JwtToken } from "../middlewares/jwt-token";

const route = express.Router();

route.get("/me", JwtToken.checkAccessToken, TweetController.myList);
route.get("/", JwtToken.checkAccessToken, TweetController.listAll);
route.get("/:username", JwtToken.checkAccessToken, TweetController.userList);
route.post("/", JwtToken.checkAccessToken, TweetService.publishTweet);

export default route;