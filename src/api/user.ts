import express from "express";

import { JwtToken } from "../middlewares/jwt-token";

import { UserService } from "../services/user";
import { UserController } from "../controllers/user";

import User from "../models/user";

const route = express.Router();

const controller = UserController;

route.get("/", JwtToken.checkAccessToken, UserController.getList);
route.get("/all", JwtToken.checkAccessToken, UserController.getAllList);
route.get("/me", JwtToken.checkAccessToken, UserController.getMe);
route.get("/:username", JwtToken.checkAccessToken, controller.retrieveUser);

route.get("/following/to", JwtToken.checkAccessToken, UserController.getFollowingToList);
route.get("/following", JwtToken.checkAccessToken, UserController.getFollowingList);
route.post("/following", JwtToken.checkAccessToken, UserController.followUser);

export default route;