import express from "express";

import { AuthService } from "../services/auth";
import { AuthController } from "../controllers/auth";

const route = express.Router();

route.post("/signup", AuthController.signup);
route.post("/login", AuthController.login);

export default route;