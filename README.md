# FakeTwitter Backend

REST Api Fake Twitter

Since the project database is stored in MongoAtlas I encourage to use same Cloud/Mongo/Platform
[Mongo Atlas](https://www.mongodb.com/cloud/atlas/register) and after that create your cluster.

After having your db in mongo atlas setting up the ```.env``` file will be easy:
 - Copy your username and password that you are using for your db cluster in mongo atlas and store in the .env file, just like this:
```
DB_USER=myusername
DB_PASS=myrandompassword
```

Then generate your JWT_TOKEN, like this:
```
JWT_KEY=somerandomkey
```

## Commands

Install all dependencies first:
```
npm install
```

Start the dev server:
```
npm start dev
```

Run tests
```
npm run dev:unit
```
